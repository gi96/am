﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ExemploCRUD.Models
{
    public class Produto
    {
        [Display(Name = "Código")]
        public int Codigo { get; set; }

        [Display(Name = "Título")]
        public string Titulo { get; set; }

        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        public int Categoria { get; set; }

        public int Plataforma { get; set; }

        public string Imagem { get; set; }
    }
}